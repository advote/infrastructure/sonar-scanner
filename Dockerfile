# This dockerfile is taken from https://github.com/newtmitch/docker-sonar-scanner. It is aimed at
# performing a sonar-scanner analysis of a project containing .ts files (and possibly other files),
# using SonarQube's "official" SonarTS analyser.

FROM node:9

RUN apt-get update
RUN apt-get install -y curl tmux maven default-jre

# Install typescript (needed by SonarTS)
WORKDIR /root
RUN npm config set strict-ssl false
RUN cd /root
RUN npm install --only=dev typescript

# Install sonar-scanner
RUN curl --insecure -o ./sonarscanner.zip -L https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip
RUN unzip sonarscanner.zip

# Configure sonar-scanner
ENV SONAR_RUNNER_HOME=/root/sonar-scanner-3.2.0.1227-linux
ENV PATH $PATH:/root/sonar-scanner-3.2.0.1227-linux/bin

CMD ["sh"]
