# Sonarqube scanner for gitlab ci building [![build status](https://gitlab.com/chvote2/infra/sonar-scanner/badges/master/build.svg)](https://gitlab.com/chvote2/infra/sonar-scanner/commits/master)

## Instructions
This image is meant to be used in the gitlab-ci pipelines of the CHVote applications.

## Content
* sonar-scanner